import { IsNotEmpty, MaxLength } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { RoleEnum } from 'src/@entities/role.entity';

export class CreateRoleDto {
  @ApiProperty({
    example: 'password',
    description: '{0:admin, 1:user}: 50, nullable: false, min-length: 5',
  })
  @MaxLength(50)
  @IsNotEmpty()
  public readonly value: RoleEnum;

  @ApiProperty({ example: 'Bunned for bad words', description: 'varchar: 200, nullable: false' })
  @MaxLength(200)
  @IsNotEmpty()
  public readonly description: string;
}
