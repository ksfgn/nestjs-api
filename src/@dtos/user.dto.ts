import { IsEmail, IsNotEmpty, IsString, MinLength } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class LoginUserDto {
  @ApiProperty({ example: 'password', description: 'varchar, nullable: false, min-length: 5' })
  @IsString()
  @IsNotEmpty()
  @MinLength(5)
  public readonly password: string;

  @ApiProperty({ example: 'test@mail.com', description: 'varchar: 100, nullable: false' })
  @IsEmail()
  public readonly email: string;
}

export class RegisterUserDto extends LoginUserDto {
  @ApiProperty({ example: 'password', description: 'varchar, nullable: false, min-length: 5' })
  @IsString()
  @IsNotEmpty()
  @MinLength(5)
  public readonly repeatPassword: string;
}
