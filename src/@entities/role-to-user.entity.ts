import { Entity, PrimaryGeneratedColumn, ManyToOne, JoinColumn } from 'typeorm';
import { User } from './user.entity';
import { Role } from './role.entity';
import { DateEntity } from 'src/utils/entity.helpers';

@Entity()
export class RoleToUser extends DateEntity {
  @PrimaryGeneratedColumn({ type: 'int' })
  public id!: number;

  @ManyToOne(() => Role, (role: Role) => role.userRoles)
  @JoinColumn({})
  public role!: Role;

  @ManyToOne(() => User, (user: User) => user.userRoles)
  @JoinColumn({})
  public user!: User;
}
