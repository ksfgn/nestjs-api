import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger/dist';
import { RoleToUser } from './role-to-user.entity';

export enum RoleEnum {
  ADMIN = 'admin',
  USER = 'user',
}

@Entity({})
export class Role {
  @ApiProperty({ example: 1 })
  @PrimaryGeneratedColumn({ type: 'int' })
  public id!: number;

  @ApiProperty({
    example: '1',
    description: '{0:admin, 1:user}: , nullable: false',
    enumName: 'RoleEnum',
  })
  @Column({
    type: 'enum',
    unique: true,
    default: RoleEnum.USER,
    enum: RoleEnum,
    nullable: false,
  })
  public value!: RoleEnum;

  @ApiProperty({ example: 'Administrator', description: 'varchar: 200, nullable: false' })
  @Column({ type: 'varchar', unique: true, length: 200, nullable: false })
  public description!: string;

  @OneToMany(() => RoleToUser, (userRoles: RoleToUser) => userRoles.role)
  userRoles!: RoleToUser[];
}
