import { Entity, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger/dist';
import { DateEntity } from 'src/utils/entity.helpers/';
import { User } from './user.entity';

@Entity({})
export class Token extends DateEntity {
  @ApiProperty({ example: 1 })
  @PrimaryGeneratedColumn({ type: 'int' })
  public id!: number;

  @ApiProperty({
    example: 'srgrdgrsggdrg43t3tfsvew4te5we.seresv4rv3rzg4tb.w3rbta4tbwatbe4tzstg',
    description: 'varchar: 500, nullable: true',
  })
  @Column({ type: 'varchar', unique: true, length: 500, nullable: true })
  public refreshToken!: string;

  @OneToOne(() => User, { cascade: true })
  @JoinColumn()
  public userId: number;
}
