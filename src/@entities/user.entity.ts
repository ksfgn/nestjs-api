import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger/dist';
import { RoleToUser } from 'src/@entities/role-to-user.entity';
import { DateEntity } from 'src/utils/entity.helpers/';

@Entity({})
export class User extends DateEntity {
  @ApiProperty({ example: 1 })
  @PrimaryGeneratedColumn({ type: 'int' })
  public id!: number;

  @ApiProperty({ example: 'test@mail.com', description: 'varchar: 100, nullable: false' })
  @Column({ type: 'varchar', unique: true, length: 100, nullable: false })
  public email!: string;

  // @ApiProperty({ example: 'password', description: 'varchar: 200, nullable: false, min-length: 5' })
  @Column({ type: 'varchar', length: 60, nullable: false })
  public password!: string;

  @ApiProperty({ example: false, description: 'boolean, default: false' })
  @Column({ type: 'boolean', nullable: false, default: false })
  public isActivated: boolean;

  @ApiProperty({
    example: `${process.env.API_URL}/addfskjakjjhfzvb4234324ljbvabgu2324peb234pusfsef`,
    description: 'varchar: , nullable: true',
  })
  @Column({ type: 'varchar', nullable: true, length: 200 })
  public activationLink: string;

  @ApiProperty({ example: false, description: 'boolean, default: false' })
  @Column({ type: 'boolean', default: false })
  public banned!: boolean;

  @ApiProperty({ example: 'bunned for bad words', description: 'varchar: , nullable: true' })
  @Column({ type: 'varchar', length: 60, nullable: true })
  public banReason: string;

  @OneToMany(() => RoleToUser, (userRoles: RoleToUser) => userRoles.user, { cascade: true })
  userRoles!: RoleToUser[];

  // addRole(role: Role) {
  //   if (this.roles == null) {
  //     this.roles = new Array<Role>();
  //   }
  //   this.roles.push(role);
  // }
}
