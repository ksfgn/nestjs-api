import { Injectable, NestMiddleware } from '@nestjs/common';
import { HttpException } from '@nestjs/common/exceptions';
import { HttpStatus } from '@nestjs/common/enums';
import { getFailureResponse } from 'src/utils/core/responses';
import { TokenService } from 'src/token/token.service';

@Injectable()
export class AuthenticationMiddleware implements NestMiddleware {
  constructor(private readonly tokenService: TokenService) {}
  use(req: any, res: any, next: () => void) {
    const authorizationHeader = req.headers.authorization;
    if (!authorizationHeader)
      throw new HttpException(
        getFailureResponse(`User Unauthorized`, HttpStatus.UNAUTHORIZED),
        HttpStatus.UNAUTHORIZED
      );

    const accessToken = authorizationHeader.split(' ')[1];
    if (!accessToken)
      throw new HttpException(
        getFailureResponse(`User Unauthorized`, HttpStatus.UNAUTHORIZED),
        HttpStatus.UNAUTHORIZED
      );

    const userData = this.tokenService.validateAccessToken(accessToken);
    if (!userData)
      throw new HttpException(
        getFailureResponse(`User Unauthorized`, HttpStatus.UNAUTHORIZED),
        HttpStatus.UNAUTHORIZED
      );

    req.user = userData;
    next();
  }
}
