import { Injectable, NestMiddleware } from '@nestjs/common';
import { HttpException } from '@nestjs/common/exceptions';
import { HttpStatus } from '@nestjs/common/enums';
import { getFailureResponse } from 'src/utils/core/responses';
import { TokenService } from 'src/token/token.service';

@Injectable()
export class AuthenticationRefreshMiddleware implements NestMiddleware {
  constructor(private readonly tokenService: TokenService) {}

  use(req: any, res: any, next: () => void) {
    const authorizationHeader = req.headers.authorization;
    if (!authorizationHeader)
      throw new HttpException(
        getFailureResponse(
          `User Unauthorized: refresh token wan't provided`,
          HttpStatus.UNAUTHORIZED
        ),
        HttpStatus.UNAUTHORIZED
      );

    const refreshToken = authorizationHeader.split(' ')[1];
    if (!refreshToken)
      throw new HttpException(
        getFailureResponse(
          `User Unauthorized: refresh token wan't provided`,
          HttpStatus.UNAUTHORIZED
        ),
        HttpStatus.UNAUTHORIZED
      );

    const userData = this.tokenService.validateRefreshToken(refreshToken);
    if (!userData)
      throw new HttpException(
        getFailureResponse(`User Unauthorized: refresh token invalid`, HttpStatus.UNAUTHORIZED),
        HttpStatus.UNAUTHORIZED
      );

    req.user = userData;
    next();
  }
}
