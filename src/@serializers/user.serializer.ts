import { User } from 'src/@entities/user.entity';
import { Serializer } from 'src/utils/core/serializer';

export const userSerializer = new Serializer<User>([
  'id',
  'email',
  'isActivated',
  'activationLink',
  'banned',
  'banReason',
  'createdAt',
  'updatedAt',
]);
