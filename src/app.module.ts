import { Module, NestModule, MiddlewareConsumer } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '@nestjs/config';
import { join } from 'path';
import { UserModule } from './user/user.module';
import { RoleModule } from './role/role.module';
import { User } from './@entities/user.entity';
import { Role } from './@entities/role.entity';
import { RoleToUser } from './@entities/role-to-user.entity';
import { TokenModule } from './token/token.module';
import { MailModule } from './mail/mail.module';
import { AuthModule } from './auth/auth.module';
import { Token } from './@entities/token.entity';
import { AuthenticationMiddleware } from './@middlewares/auth.middleware';
import { AuthenticationRefreshMiddleware } from './@middlewares/refresh.middleware';

@Module({
  controllers: [],
  providers: [],
  imports: [
    ConfigModule.forRoot({
      envFilePath: `.${process.env.NODE_ENV}.env`,
      isGlobal: true,
    }),
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: process.env.MYSQL_DB_HOST,
      port: Number(process.env.MYSQL_DB_PORT),
      username: process.env.MYSQL_DB_USERNAME,
      password: process.env.MYSQL_DB_PASSWORD,
      database: process.env.MYSQL_DB_DATABASE,
      // entities: [join(__dirname, '**', '*.entity.{ts,js}')],
      entities: [User, Role, RoleToUser, Token],
      synchronize: true,
      autoLoadEntities: true,
    }),
    UserModule,
    RoleModule,
    TokenModule,
    MailModule,
    AuthModule,
  ],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(AuthenticationMiddleware).forRoutes('api/users');
    consumer.apply(AuthenticationRefreshMiddleware).forRoutes('api/auth/logout');
    consumer.apply(AuthenticationRefreshMiddleware).forRoutes('api/auth/refresh');
  }
}
