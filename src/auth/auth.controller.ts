import { HttpStatus } from '@nestjs/common/enums';
import {
  Controller,
  Inject,
  Get,
  Param,
  Post,
  Body,
  HttpCode,
  Headers,
  Delete,
  Req,
} from '@nestjs/common/decorators';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { AuthService, IAuthorizeUserResponse } from './auth.service';
import { Response, ResponseDto } from 'src/utils/core/responses';
import { User } from 'src/@entities/user.entity';
import { RegisterUserDto, LoginUserDto } from 'src/@dtos/user.dto';

@ApiTags('Auth')
@Controller('api/auth')
export class AuthController {
  @Inject(AuthService)
  private readonly authService: AuthService;

  @ApiResponse({ status: HttpStatus.CREATED, type: User })
  @Post('/registration')
  public async registerUser(@Body() body: RegisterUserDto): Promise<IAuthorizeUserResponse> {
    return await this.authService.registerUser(body);
  }

  @ApiResponse({ status: HttpStatus.OK, type: User })
  @HttpCode(HttpStatus.OK)
  @Post('/login')
  public async loginUser(@Body() body: LoginUserDto): Promise<IAuthorizeUserResponse> {
    return await this.authService.loginUser(body);
  }

  @ApiResponse({ status: HttpStatus.OK, type: User })
  @HttpCode(HttpStatus.OK)
  @Delete('/logout')
  public async logoutUser(@Req() req): Promise<Response<null>> {
    return await this.authService.logoutUser(req.user as Partial<User>);
  }

  @ApiResponse({ status: HttpStatus.OK, type: User })
  @Get('/refresh')
  public async refreshTokens(@Req() req): Promise<IAuthorizeUserResponse> {
    return await this.authService.refreshTokens(req.user as Partial<User>);
  }

  @ApiResponse({ status: HttpStatus.ACCEPTED, type: ResponseDto<User> })
  @HttpCode(HttpStatus.ACCEPTED)
  @Get('/activate/:link')
  public async activateAccount(@Param('link') link: string) {
    return await this.authService.activateAccount(link);
  }
}
