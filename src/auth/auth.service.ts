import { HttpStatus } from '@nestjs/common/enums';
import { Injectable } from '@nestjs/common/decorators';
import { HttpException } from '@nestjs/common/exceptions';
import { InjectRepository } from '@nestjs/typeorm';
import { hashSync, compareSync } from 'bcrypt';
import { v4 as uuidV4 } from 'uuid';
import { Repository } from 'typeorm';
import { User } from 'src/@entities/user.entity';
import { getFailureResponse, getSuccessResponse, Response } from 'src/utils/core/responses';
import { userSerializer } from 'src/@serializers/user.serializer';
import { LoginUserDto, RegisterUserDto } from 'src/@dtos/user.dto';
import { MailService } from 'src/mail/mail.service';
import { ITokens, TokenService } from 'src/token/token.service';

export type IAuthorizeUserResponse = Response<{ user: Partial<User>; tokens: ITokens }>;

@Injectable()
export class AuthService {
  @InjectRepository(User)
  private readonly userRepository: Repository<User>;

  constructor(
    private readonly mailService: MailService,
    private readonly tokenService: TokenService
  ) {}

  // @Exception()
  public async registerUser(body: RegisterUserDto): Promise<IAuthorizeUserResponse> {
    const { email, password, repeatPassword } = body;

    if (password !== repeatPassword)
      throw new HttpException(
        getFailureResponse(`Passwords don't match`, HttpStatus.BAD_REQUEST),
        HttpStatus.BAD_REQUEST
      );

    const userDB = await this.userRepository.findOneBy({ email });
    if (userDB)
      throw new HttpException(
        getFailureResponse(`User with '${email}' has been registered`, HttpStatus.CONFLICT),
        HttpStatus.CONFLICT
      );

    const hashPassword = hashSync(password, 5);
    const activationLink = uuidV4();

    const user = await this.userRepository.create({
      email,
      password: hashPassword,
      activationLink,
    });

    const responseUser = await this.userRepository.save(user);

    await this.mailService.sendActivationMail(
      email,
      `${process.env.API_URL}/api/auth/activate/${activationLink}`
    );

    const serializedUser = userSerializer.getSerializedObject(responseUser);

    const tokens = this.tokenService.generateTokens(serializedUser);
    await this.tokenService.saveToken(user.id, tokens.refreshToken);

    const responseData = { user: serializedUser, tokens };

    return getSuccessResponse(
      responseData,
      HttpStatus.CREATED,
      `User with '${email}' successfully registered`
    );
  }
  // @Exception()
  public async loginUser(body: LoginUserDto): Promise<IAuthorizeUserResponse> {
    const { email, password } = body;

    const userDB = await this.userRepository.findOneBy({ email });
    if (!userDB)
      throw new HttpException(
        getFailureResponse(`User with '${email}' doesn't exist`, HttpStatus.NOT_FOUND),
        HttpStatus.NOT_FOUND
      );

    const isPasswordsMatch = compareSync(password, userDB.password);
    if (!isPasswordsMatch)
      throw new HttpException(
        getFailureResponse(`Incorrect password`, HttpStatus.BAD_REQUEST),
        HttpStatus.BAD_REQUEST
      );

    const serializedUser = userSerializer.getSerializedObject(userDB);

    const tokens = this.tokenService.generateTokens(serializedUser);
    await this.tokenService.saveToken(userDB.id, tokens.refreshToken);

    const responseData = { user: serializedUser, tokens };

    return getSuccessResponse(responseData, HttpStatus.OK, `User with successfully logged in`);
  }

  public async refreshTokens(userData: Partial<User>): Promise<IAuthorizeUserResponse> {
    const userDB = await this.userRepository.findOne({ where: { id: userData.id } });

    const serializedUser = userSerializer.getSerializedObject(userDB);

    const tokens = this.tokenService.generateTokens(serializedUser);
    await this.tokenService.saveToken(userDB.id, tokens.refreshToken);

    const responseData = { user: serializedUser, tokens };

    return getSuccessResponse(
      responseData,
      HttpStatus.OK,
      `User's tokens were successfully refreshed`
    );
  }

  public async logoutUser(userData: Partial<User>): Promise<Response<null>> {
    const refreshTokenDB = await this.tokenService.findTokenByUserId(userData.id);
    await this.tokenService.removeToken(refreshTokenDB.refreshToken);

    return getSuccessResponse(null, HttpStatus.OK, `User successfully logged out`);
  }

  // @Exception()
  public async activateAccount(link: string) {
    const user = await this.userRepository.findOne({ where: { activationLink: link } });

    if (!user)
      throw new HttpException(
        getFailureResponse("Searched user wasn't found", HttpStatus.NOT_FOUND),
        HttpStatus.NOT_FOUND
      );

    user.isActivated = true;
    this.userRepository.save(user);
    return getSuccessResponse(
      userSerializer.getSerializedObject(user),
      HttpStatus.ACCEPTED,
      `Account was successfully activated`
    );
  }
}
