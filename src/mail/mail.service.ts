import { Injectable } from '@nestjs/common';
import { createTransport } from 'nodemailer';

@Injectable()
export class MailService {
  private transport = createTransport({
    port: Number(process.env.SMTP_PORT),
    host: process.env.SMTP_HOST,
    secure: false,
    auth: {
      user: process.env.SMTP_USER,
      pass: process.env.SMTP_PASSWORD,
    },
    // tls: {
    //    rejectUnauthorized: false,
    // Error: self-signed certificate in certificate chain // TODO enable prev line to avoid this error, or problem with your own network//(may be antivirus cause it like in my case)
    // },
  });

  public async sendActivationMail(to, link) {
    await this.transport.sendMail(
      {
        from: process.env.SMTP_USER,
        to,
        subject: `Activation of your account on ${process.env.API_URL}`,
        text: '',
        html: `
              <div>
                  <h2>To active your account go via the link</h2>
                  <p><a href="${link}">Active</a></p>
              </div>
            `,
      },
      (err, info) => {
        if (err)
          return console.log({
            text: 'mail err',
            err,
          });
      }
    );
    return;
  }
}
