import { NestFactory } from '@nestjs/core';
import { ValidationPipe } from '@nestjs/common/pipes';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger/dist';
import { ErrorFilter } from './utils/core/responses';

async function bootstrap() {
  const PORT = process.env.PORT || 5000;
  const app = await NestFactory.create(AppModule);

  app.useGlobalPipes(new ValidationPipe({ whitelist: true, transform: true }));

  const config = new DocumentBuilder()
    .setTitle('Nest Js first Api')
    .setDescription('The best backend')
    .setVersion('1.0.0')
    .addTag('Oleksandr Shapovalov')
    .build();

  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('/api/docs', app, document);

  app.useGlobalFilters(new ErrorFilter());

  await app.listen(PORT, () => console.log(`Server started with ${PORT} port`));
}
bootstrap();
