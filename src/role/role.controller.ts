import { Controller, Inject, Post, Body, Get, Param, Query, Req } from '@nestjs/common/decorators';
import { HttpStatus } from '@nestjs/common/enums';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { CreateRoleDto } from 'src/@dtos/role.dto';
import { Role, RoleEnum } from 'src/@entities/role.entity';
import { RoleService } from './role.service';
import { Response } from 'src/utils/core/responses';

@ApiTags('Roles')
@Controller('api/roles')
export class RoleController {
  @Inject(RoleService)
  private readonly roleService: RoleService;

  @ApiResponse({ status: HttpStatus.CREATED, type: Role })
  @Post()
  async createRole(@Body() body: CreateRoleDto): Promise<Response<Role>> {
    return await this.roleService.createRole(body);
  }

  @ApiResponse({ status: HttpStatus.OK, type: Role })
  @Get('/:value')
  async getRoleByValue(@Param('value') value: RoleEnum): Promise<Response<Role>> {
    return await this.roleService.getRoleByValue(value);
  }
}
