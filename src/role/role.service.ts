import { InjectRepository } from '@nestjs/typeorm';
import { HttpStatus } from '@nestjs/common/enums';
import { Injectable } from '@nestjs/common/decorators';
import { HttpException } from '@nestjs/common/exceptions';
import { getFailureResponse, Response, getSuccessResponse } from 'src/utils/core/responses';
import { Repository } from 'typeorm';
import { CreateRoleDto } from '../@dtos/role.dto';
import { Role, RoleEnum } from '../@entities/role.entity';

@Injectable()
export class RoleService {
  @InjectRepository(Role)
  private readonly roleRepository: Repository<Role>;

  // @Exception()
  public async createRole(body: CreateRoleDto): Promise<Response<Role>> {
    const roleDb = await this.roleRepository.findOne({ where: { value: body.value } });
    if (roleDb)
      throw new HttpException(
        getFailureResponse(`Role '${body.value}' has already existed`, HttpStatus.CONFLICT),
        HttpStatus.CONFLICT
      );

    const role = await this.roleRepository.create(body);

    return getSuccessResponse(
      await this.roleRepository.save(role),
      HttpStatus.CREATED,
      `Role '${role.value}' was successfully created`
    );
  }

  // @Exception()
  public async getRoleByValue(value: RoleEnum): Promise<Response<Role>> {
    const role = await this.roleRepository.findOne({ where: { value } });

    if (!role)
      throw new HttpException(
        getFailureResponse(`Role '${value}' not found`, HttpStatus.NOT_FOUND),
        HttpStatus.NOT_FOUND
      );

    return getSuccessResponse(role, HttpStatus.OK, `Role '${value}' was successfully received`);
  }
}
