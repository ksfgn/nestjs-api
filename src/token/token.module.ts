import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { TokenService } from './token.service';
import { Token } from 'src/@entities/token.entity';

@Module({
  providers: [TokenService],
  exports: [TokenService],
  imports: [TypeOrmModule.forFeature([Token])],
})
export class TokenModule {}
