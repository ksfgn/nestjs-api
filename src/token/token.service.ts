import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm/';
import * as jwt from 'jsonwebtoken';
import { Repository } from 'typeorm';
import { Token } from 'src/@entities/token.entity';
import { User } from 'src/@entities/user.entity';
import { Exception } from 'src/utils/docorators';

export interface ITokens {
  accessToken: string;
  refreshToken: string;
}

@Injectable()
export class TokenService {
  @InjectRepository(Token)
  private readonly tokenRepository: Repository<Token>;

  public generateTokens(payload: Partial<User>) {
    const accessToken = jwt.sign(payload, process.env.JWT_ACCESS_SECRET, {
      expiresIn: '15m',
    });
    const refreshToken = jwt.sign(payload, process.env.JWT_REFRESH_SECRET, {
      expiresIn: '30d',
    });
    return {
      accessToken,
      refreshToken,
    };
  }

  public validateAccessToken(accessToken: string): Partial<User> | null {
    try {
      const userData = jwt.verify(accessToken, process.env.JWT_ACCESS_SECRET);
      return userData as Partial<User>;
    } catch (e) {
      return null;
    }
  }
  public validateRefreshToken(refreshToken: string): Partial<User> | null {
    try {
      const userData = jwt.verify(refreshToken, process.env.JWT_REFRESH_SECRET);
      return userData as Partial<User>;
    } catch (e) {
      return null;
    }
  }

  // @Exception()
  public async saveToken(userId: number, refreshToken: string) {
    const tokenData = await this.tokenRepository.findOne({ where: { userId } });
    if (tokenData) {
      tokenData.refreshToken = refreshToken;
      return this.tokenRepository.save(tokenData);
    }
    const newRefreshToken = this.tokenRepository.create({ refreshToken, userId });
    return await this.tokenRepository.save(newRefreshToken);
  }

  // @Exception()
  public async removeToken(refreshToken: string) {
    const tokenData = await this.tokenRepository.delete({ refreshToken });
    return tokenData;
  }

  // @Exception()
  public async findToken(refreshToken: string) {
    const tokenData = await this.tokenRepository.findOne({ where: { refreshToken } });
    return tokenData;
  }
  public async findTokenByUserId(userId: number) {
    const tokenData = await this.tokenRepository.findOne({ where: { userId } });
    return tokenData;
  }
}
