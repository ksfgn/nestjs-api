import { Controller, Inject, Post, Body, Get, Param, Query, Req } from '@nestjs/common/decorators';
import { HttpStatus } from '@nestjs/common/enums';
import { ParseIntPipe } from '@nestjs/common/pipes';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { PaginationResponse, Response, ResponseDto } from 'src/utils/core/responses';
import { QueryDto } from 'src/utils/dtos.helpers';
import { User } from 'src/@entities/user.entity';
import { UserService } from './user.service';

//====================================================
@ApiTags('Users')
@Controller('api/users')
export class UserController {
  @Inject(UserService)
  private readonly userService: UserService;

  @ApiOperation({})
  @ApiResponse({ status: HttpStatus.OK, type: ResponseDto<User> })
  @Get(':id')
  public async getOneUser(@Param('id', ParseIntPipe) id: number): Promise<Response<Partial<User>>> {
    return await this.userService.getOneUser(id);
  }

  @ApiOperation({ parameters: [] }) // TODO add query parameters
  @ApiResponse({
    status: HttpStatus.OK,
    type: [User],
  })
  @Get()
  public async getAllUsers(
    @Req() req: Request,
    @Query() query: QueryDto
  ): Promise<PaginationResponse<Partial<User>>> {
    return await this.userService.getAllUsers(query, req.path.split('/').pop());
  }
}
