import { HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { HttpException } from '@nestjs/common/exceptions';
import { Like, Repository } from 'typeorm';
import { User } from 'src/@entities/user.entity';
import {
  getFailureResponse,
  getPaginateResponseData,
  getSuccessResponse,
} from 'src/utils/core/responses';
import { PaginateData, PaginationResponse, Response } from 'src/utils/core/responses/types';
import { userSerializer } from 'src/@serializers/user.serializer';

@Injectable()
export class UserService {
  @InjectRepository(User)
  private readonly userRepository: Repository<User>;

  // @Exception()
  public async getOneUser(id: number): Promise<Response<Partial<User>>> {
    const user = await this.userRepository.findOneBy({ id });
    if (!user)
      throw new HttpException(
        getFailureResponse(`User with id ${id} not found`, HttpStatus.NOT_FOUND),
        HttpStatus.NOT_FOUND
      );

    return getSuccessResponse(
      userSerializer.getSerializedObject(user),
      HttpStatus.OK,
      `User '${user.email}' was successfully received`
    );
  }

  // @Exception()
  public async getAllUsers(query, route: string): Promise<PaginationResponse<Partial<User>>> {
    const take: number = query.take || 10;
    const page: number = query.page || 1;
    const skip: number = (page - 1) * take;
    const keyword: string = query.keyword || '';

    const userData: PaginateData<User> = await this.userRepository.findAndCount({
      where: { email: Like('%' + keyword + '%') },
      order: { id: 'DESC' },
      take: take,
      skip: skip,
    });

    const { results, metadata } = getPaginateResponseData<User>(
      route,
      userData,
      page,
      take,
      keyword
    );

    const paginateResponseData = {
      results: userSerializer.getSerializedObjectsArr(results),
      metadata,
    };

    return getSuccessResponse(
      paginateResponseData,
      HttpStatus.OK,
      `Users were successfully received`
    );
  }
}
