import { ExceptionFilter, Catch, HttpException, ArgumentsHost, HttpStatus } from '@nestjs/common';
import { getFailureResponse } from 'src/utils/core/responses';

@Catch()
export class ErrorFilter implements ExceptionFilter {
  catch(error: any | Error, host: ArgumentsHost) {
    const response = host.switchToHttp().getResponse();
    const status =
      error instanceof HttpException ? error.getStatus() : HttpStatus.INTERNAL_SERVER_ERROR;

    if (status === HttpStatus.INTERNAL_SERVER_ERROR) {
      return response.status(status).send(getFailureResponse());
    } else {
      response.status(status).send(error.response);
    }
  }
}
