import { ApiProperty } from '@nestjs/swagger';
import { HttpStatus } from '@nestjs/common/enums';
import { IGetSuccessResponse } from './types';

export class ResponseDto<T> implements IGetSuccessResponse<T> {
  @ApiProperty()
  public readonly data: T;

  @ApiProperty()
  public readonly message: string;

  @ApiProperty()
  public readonly statusCode: HttpStatus;

  @ApiProperty()
  public readonly success: true;
}
