import { HttpStatus } from '@nestjs/common/enums';
import {
  IGetFailureResponse,
  IGetPaginateResponseData,
  IGetSuccessResponse,
  PaginateData,
} from './types';

export const getSuccessResponse = <T>(
  data: T,
  code = HttpStatus.OK,
  message = 'Success Response'
): IGetSuccessResponse<T> => ({
  success: true,
  data: data,
  statusCode: code,
  message: message,
});

export const getFailureResponse = (
  message = 'Network Error',
  code = HttpStatus.INTERNAL_SERVER_ERROR
): IGetFailureResponse => ({
  success: false,
  data: null,
  statusCode: code,
  message: message,
});

export function getPaginateResponseData<T>(
  route: string,
  data: PaginateData<T>,
  page: number,
  take: number,
  keyword: string
): IGetPaginateResponseData<T> {
  const [result, total] = data;
  const lastPage: number = Math.ceil(total / take);

  const isNextPage: boolean = page + 1 > lastPage;
  const isPrevPage: boolean = page - 1 < 1;

  const nextPage: number | null = isNextPage ? null : page + 1;
  const prevPage: number | null = isPrevPage ? null : page - 1;

  const nextLink: string | null = isNextPage
    ? null
    : `${route}?page=${nextPage}&take=${take}&keyword=${keyword}`;

  const prevLink: string | null = isPrevPage
    ? null
    : `${route}?page=${nextPage}&take=${take}&keyword=${keyword}`;

  return {
    results: [...result],
    metadata: {
      count: total,
      currentPage: page,
      nextPage: nextPage,
      prevPage: prevPage,
      lastPage: lastPage,
      nextLink: nextLink,
      prevLink: prevLink,
    },
  };
}
