export interface IGetSuccessResponse<T> {
  success: true;
  data: T;
  statusCode: number;
  message: string;
}

export interface IGetFailureResponse {
  success: false;
  data: null;
  statusCode: number;
  message: string | string[];
}

export interface IGetPaginateResponseData<T> {
  results: T[];
  metadata: {
    count: number;
    currentPage: number;
    nextPage: number;
    prevPage: number | null;
    lastPage: number | null;
    nextLink: string | null;
    prevLink: string | null;
  };
}

export type IGetSuccessResponsePagination<T> = IGetSuccessResponse<IGetPaginateResponseData<T>>;

export type PaginateData<T> = [T[], number];

export type PaginationResponse<T> = IGetSuccessResponsePagination<T> | IGetFailureResponse;

export type Response<T> = IGetSuccessResponse<T> | IGetFailureResponse;
