export class Serializer<T> {
  private fields: (keyof T)[] = [];

  constructor(fields: (keyof T)[]) {
    this.setFields(fields);
  }

  setFields(fields: (keyof T)[]) {
    this.fields = fields;
  }

  getSerializedObject(object: T) {
    if (!object || !this.fields.length) {
      throw Error("You didn't pass an object or serializer fields array is empty");
    }

    const newObj: Partial<T> = {};

    this.fields.forEach((field) => {
      newObj[field] = object[field];
    });

    return newObj;
  }

  getSerializedObjectsArr(objectsArr: T[]) {
    const newObjArr: Partial<T>[] = [];

    objectsArr.forEach((obj) => {
      newObjArr.push(this.getSerializedObject(obj));
    });

    return newObjArr;
  }
}
