import { getFailureResponse } from 'src/utils/core/responses';

export function Exception() {
  return (target: any, nameMethod: string, descriptor: PropertyDescriptor) => {
    const originalMethod = descriptor.value;
    descriptor.value = async function (...args: any[]) {
      try {
        const executionMethod = await originalMethod.apply(this, args);
        return executionMethod;
      } catch (error) {
        return getFailureResponse(error.message);
      }
    };
  };
}
