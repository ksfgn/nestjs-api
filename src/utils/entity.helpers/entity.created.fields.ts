import { ApiProperty } from '@nestjs/swagger';
import { CreateDateColumn, UpdateDateColumn } from 'typeorm';

export abstract class DateEntity {
  @ApiProperty({ example: new Date().toISOString() })
  @CreateDateColumn({ type: 'timestamp' })
  public createdAt!: Date;

  @ApiProperty({ example: new Date().toISOString() })
  @UpdateDateColumn({ type: 'timestamp' })
  public updatedAt!: Date;
}
